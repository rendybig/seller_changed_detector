# Created by Rendy Wang on 2020/01/10
import time
import json
import requests
import sys
from selenium import webdriver
import script


lost_buybox = {
    'NA': [],
    'EU': [],
    'APAC': [],
    'JP': []
}
js = script.change_address
URL = 'https://www.amazon{}/Furbo-Dog-Camera-Designed-Compatible/dp/{}'
dic = {
    'US': ['.com', 'B01FXC7JWQ'],
    'CA': ['.ca', 'B01FXC7JWQ'],
    'MX': ['.com.mx', 'B01FXC7JWQ'],
    'GB': ['.co.uk', 'B01FXC7JWQ'],
    'DE': ['.de', 'B01FXC7JWQ'],
    'FR': ['.fr', 'B01FXC7JWQ'],
    'IT': ['.it', 'B01FXC7JWQ'],
    'ES': ['.es', 'B01FXC7JWQ'],
    'NL': ['.nl', 'B01FXC7JWQ'],
    'SE': ['.se', 'B01FXC7JWQ'],
    'JP': ['.co.jp', 'B01FXC7JWQ'],
    'AU': ['.com.au', 'B01FXC7JWQ'],
    'SG': ['.sg', 'B085W7LHM4']
}
postal_code = {
    'US': '10542',
    'AU': '2222',
    'ES': '22111',
    'CA': ['A1A', '1A1'],
    'MX': '20010',
    'GB': 'PO16 7GZ',
    'FR': '75008',
    'IT': '00118',
    'DE': '26133',
    'JP': ['540','0002'],
    'SE': ['114','55'],
    'SG': '247964'
}
email_list = {
    'NA': ['lostbuybox_na@tomofun.com'],
    'EU': ['lostbuybox_eu@tomofun.com'],
    'APAC': ['lostbuybox_apac@tomofun.com'],
    'JP': ['lostbuybox_jp@tomofun.com']
}
country_list = {
    'NA': ['US', 'CA', 'MX'],
    'EU': ['GB', 'DE', 'FR', 'IT', 'ES', 'NL', 'SE'], # DE
    'APAC': ['SG', 'AU'],
    'JP': ['JP']
}


def send_email(lost, region):
    if len(lost[region]) > 0:
        email = email_list[region]
        e = {
            'Content': 'Hi,\n\nOur buybox in ' + str(lost[region]).replace("'", "").strip(
                "[]") + ' seems to disappear,\nPlease take a look.\n\nBest,\nTomofun IT',
            'Title': 'Our buybox in ' + str(lost[region]).replace("'", "").strip("[]") + ' seems to disappear'
        }
        print(e)
        time.sleep(5)
        _params = {
            'Reciever': email,
            'Content': e.get('Content'),
            'Title': e.get('Title'),
        }
        _params = json.dumps(_params)
        re = requests.post(
            'https://script.google.com/macros/s/AKfycbz0hTzi9KA8-QM0KwNpIC8w-KcmIpjhn2jSa8xmpvDJbGpH73SU/exec',
            headers={'Content-Type': 'application/json; charset=utf-8'},
            data=_params)
        log.write('{}\n'.format(re.text))


region = sys.argv[1]
if region == 'EU':
    log = open('buybox_detect.log', mode='w', encoding='utf8')
    log.write(time.strftime("%Y-%m-%d", time.localtime()) + "\n")
else:
    log = open('buybox_detect.log', mode='a', encoding='utf8')

for country in country_list[region]:
    has_bb = 0
    url = URL.format(dic[country][0], dic[country][1])
    for i in range(5):
        driver = webdriver.Firefox()
        driver.get(url)
        time.sleep(2)
        driver.refresh()
        try:
            if country in ['AU']:
                driver.refresh()
                time.sleep(3)
                driver.find_element_by_xpath('//*[@id="glow-ingress-line2"]').click()
                time.sleep(3)
                driver.find_element_by_xpath('//*[@id="GLUXPostalCodeWithCity_PostalCodeInput"]').send_keys(postal_code[country])
                time.sleep(1)
                driver.find_element_by_xpath('//*[@id="GLUXPostalCodeWithCity_CityValue"]').click()
                driver.find_element_by_xpath('//*[@id="GLUXPostalCodeWithCity_DropdownList_0"]').click()
                driver.find_element_by_xpath('//*[@id="GLUXPostalCodeWithCityApplyButton"]/input').click()
                time.sleep(2)
            elif country in ['US', 'ES', 'MX', 'GB', 'FR', 'IT', 'DE', 'SG']:
                driver.refresh()
                time.sleep(3)
                driver.find_element_by_xpath('//*[@id="glow-ingress-line2"]').click()
                time.sleep(3)
                driver.find_element_by_xpath('//*[@id="GLUXZipUpdateInput"]').send_keys(postal_code[country])
                time.sleep(1)
                driver.find_element_by_xpath('//*[@id="GLUXZipUpdate"]/span/input').click()
                time.sleep(2)
                driver.refresh()
            elif country in ['CA', 'JP', 'SE']:
                driver.refresh()
                time.sleep(3)
                driver.find_element_by_xpath('//*[@id="glow-ingress-line2"]').click()
                time.sleep(3)
                driver.find_element_by_xpath('//*[@id="GLUXZipUpdateInput_0"]').send_keys(postal_code[country][0])
                driver.find_element_by_xpath('//*[@id="GLUXZipUpdateInput_1"]').send_keys(postal_code[country][1])
                time.sleep(1)
                driver.find_element_by_xpath('//*[@id="GLUXZipUpdate"]/span/input').click()
                time.sleep(2)
                driver.refresh()
            else:
                driver.execute_script(js.format(dic=dic[country], country=country))
                time.sleep(1)
                driver.refresh()
        except:
            driver.refresh()
        
        
        driver.execute_script("window.scrollTo(0,450)")
        time.sleep(1)

        xpaths = [
            '//*[@id="sellerProfileTriggerId"]',
            '//*[@id="tabular-buybox-truncate-1"]/span[2]/span',
            '//*[@id="buyboxTabularTruncate-1"]/span[2]',
            '//*[@id="snsAccordionRowMiddle"]/div/div[2]/div/div[7]/span',
            '//*[@id="sfsb_accordion_head"]/div[2]/div/span[2]'
        ]
        results = []
        for xpath in xpaths:
            try:
                results.append(driver.find_element_by_xpath(xpath).text)
            except Exception as e:
                results.append('No buybox')
            time.sleep(1)
        print(country, results)

        for res in results:
            if ('Furbo' in res):
                has_bb = 1
        driver.quit()
        if has_bb == 1:
            break

    log.write('{c},{b}\n'.format(c=country, b=results))
    if has_bb == 0:
        if country in ['US', 'CA', 'MX']:
            lost_buybox['NA'].append(country)
        elif country in ['AU', 'SG']:
            lost_buybox['APAC'].append(country)
        elif country == 'JP':
            lost_buybox['JP'].append(country)
        else:
            lost_buybox['EU'].append(country)

send_email(lost_buybox, region)